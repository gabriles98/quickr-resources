<?php

#VARIABILI ESSENZIALE PER TUTTE LE CLASSI
static $null = 0;


#ACCESSO OAUTH A FACEBOOK
Class FacebookAccess {

    #REQUIRED PUBLIC VARS

    public $conn;

    protected function ConnectMainDb() {
        require(__DIR__ . "/../registration/login/standard/db_config.php");

        $this->conn = mysql_connect($host,$user,$password) OR die("Impossibile connettersi al database");
        mysql_select_db($db, $this->conn);
    }

    protected function ConnectFacebookDb() {
        require(__DIR__ . "/fb_config.php");

        $this->conn = mysql_connect($fb_host,$fb_user,$fb_password) OR die("Impossibile connettersi al database");
        mysql_select_db($fb_db, $this->conn);
    }


}

#FUNZIONI E CARICAMENTO DATI SU DB PER PIATTAFORMA FACEBOOK
Class FacebookData extends FacebookAccess {

}

#ACCESSO OAUTH A TWITTER
Class TwitterAccess {
    #REQUIRED PUBLIC VARS FOR TWITTER
    public $conn;


    protected function ConnectMainDb() {
        require(__DIR__ . "/../registration/login/standard/db_config.php");

        $this->conn = mysql_connect($host,$user,$password) OR die("Impossibile connettersi al database");
        mysql_select_db($db, $this->conn);
    }

    protected function ConnectFacebookDb() {
        require(__DIR__ . "/tw_config.php");

        $this->conn = mysql_connect($tw_host,$tw_user,$tw_password) OR die("Impossibile connettersi al database");
        mysql_select_db($tw_db, $this->conn);
    }
}

#FUNZIONI E CARICAMENTO DATI SU DB PER PIATTAFORMA TWITTER
Class TwitterData extends TwitterAccess {

}

#ACCESSO OAUTH A GOOGLE
Class TumblrAccess {
#REQUIRED PUBLIC VARS FOR TWITTER
    public $conn;


    protected function ConnectMainDb() {
        require(__DIR__ . "/../registration/login/standard/db_config.php");

        $this->conn = mysql_connect($host,$user,$password) OR die("Impossibile connettersi al database");
        mysql_select_db($db, $this->conn);
    }

    protected function ConnectFacebookDb() {
        require(__DIR__ . "/um_config.php");

        $this->conn = mysql_connect($um_host,$um_user,$um_password) OR die("Impossibile connettersi al database");
        mysql_select_db($um_db, $this->conn);
    }
}

#FUNZIONI E CARICAMENTO DATI SU DB PER PIATTAFORMA TUMBLR
Class TumblrData extends TumblrAccess {

}

#ACCESSO OAUTH A TUMBLR
Class GoogleAccess {
#REQUIRED PUBLIC VARS FOR TWITTER
    public $conn;


    protected function ConnectMainDb() {
        require(__DIR__ . "/../registration/login/standard/db_config.php");

        $this->conn = mysql_connect($host,$user,$password) OR die("Impossibile connettersi al database");
        mysql_select_db($db, $this->conn);
    }

    protected function ConnectFacebookDb() {
        require(__DIR__ . "/gp_config.php");

        $this->conn = mysql_connect($gp_host,$gp_user,$gp_password) OR die("Impossibile connettersi al database");
        mysql_select_db($gp_db, $this->conn);
    }
}



#FUNZIONI E CARICAMENTO DATI SU DB PER PIATTAFORMA GOOGLE (PLUS, DRIVE, DOCS, ECC...)
Class GoogleData extends GoogleAccess {

}