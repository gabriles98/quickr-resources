
<?php
require(__DIR__ . '/../registration/login/standard/authentication.class.php');
$auth = new UserAuthentication();
$auth->IsAuth();

//VARIABILI BASE
$user = $auth->ShowUsername();

//VERIFICA LOGIN E ACCESSO AREA PRIVATA


class Installer {

    public $zero = 0;
    public $conn;

    protected function MkDir() {
        $id = $this->ShowId();
        $this->ConnectDb();
        $userfolder= md5($id);

        if (is_dir("../../storage/users".$userfolder."")) {
            mkdir(__DIR__.'/../../storage/users/'.$userfolder, 0777);
            mkdir(__DIR__.'/../../storage/users/'.$userfolder.'/profile',0777);
            mkdir(__DIR__.'/../../storage/users/'.$userfolder.'/galleries',0777);
            mkdir(__DIR__.'/../../storage/users/'.$userfolder.'/galleries/default',0777);
            mkdir(__DIR__.'/../../storage/users/'.$userfolder.'/files',0777);
            mkdir(__DIR__.'/../../storage/users/'.$userfolder.'/data',0777);
            //FINE CREAZIONE CARTELLE PER L'UTENTE
        }
        else
        {

        }
    }

    //CONNESSIONE DB
    final protected function ConnectDb() {
        require(__DIR__ . "/../registration/login/standard/db_config.php");

        $this->conn = mysql_connect($host,$user,$password) OR die("Impossibile connettersi al database");
        mysql_select_db($db, $this->conn);
    }

    public function CheckSign($zero) {
        $this->ConnectDb();
        $sql = "SELECT ver FROM users WHERE id=$_SESSION[user_id]";
        $res = mysql_query($sql,$this->conn);
        if ((mysql_result($res, $zero)) == 1) {

        }
        elseif ((mysql_result($res, $zero)) == 0) {
            header('Location: ../../ui/login/loginRegister.php?alert=ver');
        }
    }

    public function CheckVer($zero) {
        $this->ConnectDb();
        $sql = "SELECT keyl FROM users WHERE id=$_SESSION[user_id]";
        $res = mysql_query($sql,$this->conn);
        if ((mysql_result($res, $zero)) == 1) {
              header('Location: ../../../ui/private/overview.php');
             }
        elseif ((mysql_result($res, $zero)) == 0) {

        }
    }

    //CERCO L'ID
    protected function ShowId()
    {
        $this->ConnectDb();
        $sql = "SELECT id FROM users WHERE id=$_SESSION[user_id]";
        $res = mysql_query($sql,$this->conn);
        $row = mysql_fetch_array($res);
        mysql_close($this->conn);
        return $row['id'];
    }


    //CLASSE PER SWITCH DI CONTROLLO UTENTE
    public function Installing() {
        $id = $this->ShowId();
        $this->ConnectDb();
        $sql = "SELECT keyl FROM users WHERE id='$id'";
        $res = mysql_query($sql,$this->conn);
        $pns = mysql_result($res, 0);
        //SWITCH PER GESTIONE CODICE DI VERIFICA INSTALL.PHP
        switch ($pns) {
            case 0:
                break;

            case 1:
                header("Location: ../../ui/private/overview.php");
                break;
        }


    }

    //INSTALLAZIONE STEP 1 - DATI ANAGRAFICI


    public function HtmlStep1() {
        $this->ConnectDb();

        switch($_GET['step']) {

            case 0:
                #SWITCH PER GESTIONE PROCEDURE CASE '0'
                switch ($_GET['procedure']) {
                    case "profilebase":
                     echo '
                        <form class="form-wrapper-01" action="installer.class.php?step=0&procedure=profilebase_process" method="post">
                        <input class="inputbox" type="text" name="name" placeholder="Il tuo nome*" required>
                        <input class="inputbox" type="text" name="surname" placeholder="Il tuo cognome" required>
                        <button type="submit" class="cld">Prosegui</button>
                        ';
                    break;
                    case "profilebase_process":
                         $id = $this->ShowId();
                         $this->ConnectDb();

                         $sql="REPLACE INTO settings SET id='$id', nome='$_POST[name]', cognome='$_POST[surname]'";
                         $retval = mysql_query($sql,$this->conn);
                         if(!$retval)
                         {
                             die('Non posso inserire i dati. Descrizione Errore: ' . mysql_error());
                         }
                         mysql_close($this->conn);
                         header('Location: installer.class.php?step=0&procedure=location');
                    break;
                    case "location":
                        echo '

                        <form class="form-wrapper-01" action="installer.class.php?step=0&procedure=location_process" method="post">
                        <input class="inputbox" type="text" name="city" placeholder="In che città vivi?*" required>
                        <input class="inputbox" type="text" name="country" placeholder="In che stato vivi?*" required>
                        <button type="submit" class="cld">Prosegui</button>
                        ';
                    break;

                    case "location_process":
                        $id = $this->ShowId();
                        $this->ConnectDb();
                            $city=$_POST['city'];
                            $country=$_POST['country'];
                        $sql="UPDATE settings SET city='$city', state='$country'
                              WHERE id='$id'";
                        $retval = mysql_query($sql,$this->conn);
                        if(!$retval)
                        {
                            die('Non posso inserire i dati. Descrizione Errore: ' . mysql_error());
                        }
                        mysql_close($this->conn);
                        echo '<script type="text/javascript">window.location = "installer.class.php";</script>';

                    break;

                    case "extraprofile":
                        echo '
                        <form class="form-wrapper-01" action="installer.class.php?step=0&procedure=extraprofile_process" method="post">
                        <label class="le15" for="sex">Di che sesso sei?</label>
                        <select class="inputbox" name="sex" style="height: 40px; width: 348px;" required> <!-- DA METTERE APOSTO CSS: DIEGO! -->
                            <option value="m">Maschio</option>
                            <option value="f">Femmina</option>
                        </select>
                        <br /><br />
                        <label for="age" class="le15">Il tuo anno di nascita:</label><br />
                        <input class="inputbox" type="date" name="age" placeholder="Quando sei nato?*" required>
                        <button type="submit" class="cld">Prosegui</button>
                        ';
                    break;

                    case "extraprofile_process":
                        $id = $this->ShowId();
                        $this->ConnectDb();
                            $age= $_POST['age'];
                            $sex= $_POST['sex'];

                        function age($date){
                        $year_diff = '';
                        $time = strtotime($date);
                        if(FALSE === $time){
                            return '';

                        }
                        $date = date('Y-m-d', $time);
                        list($year,$month,$day) = explode("-",$date);
                        $year_diff = date("Y") - $year;
                        $month_diff = date("m") - $month;
                        $day_diff = date("d") - $day;
                        if ($day_diff < 0 || $month_diff < 0)
                        {
                            $year_diff--;
                        }
                        elseif  ($day_diff > 0 || $month_diff > 0){

                        }

                        return $year_diff;
                    }

                        //SALVO NELLA VARIABILE $eta L'ETA' EFFETTIVA DELLA PERSONA
                        $eta = age($_POST['age']);

                        //AVVIO CODICE SQL
                        $sql="UPDATE settings SET age='$age', sesso='$sex', eta='$eta'
                               WHERE id='$id'";
                        $retval = mysql_query($sql,$this->conn);
                        if(!$retval)
                        {
                            die('Non posso inserire i dati. Descrizione Errore: ' . mysql_error());
                        }


                        //CHIUDO LA CONNESSIONE AL DB MYSQL E AL PARAMETRO CONN RELATIVO A DbConnect()
                        mysql_close($this->conn);

                        echo '<script type="text/javascript">window.location = "installer.class.php";</script>';
                        break;

                    case "moreprofile":

                        echo '
                        <form class="form-wrapper-01" action="installer.class.php?step=0&procedure=moreprofile_process" method="post">
                        <input class="inputbox" type="text" name="work" placeholder="Che lavoro fai? Se sei studente non rispondere." required>
                        <input class="inputbox" type="text" name="school" placeholder="In che scuola studi o hai studiato?*" required>
                        <button type="submit" class="cld">Prosegui</button>
                        ';
                        break;

                    case "moreprofile_process":
                        $id = $this->ShowId();
                        $this->ConnectDb();
                        if ($_POST['work']=="" OR $_POST['age']="niente") {
                            $work="Studente";
                        } else
                        {
                            $work=$_POST['work'];
                        }
                        $sql="UPDATE settings SET work='$work', school='$_POST[school]' WHERE id='$id'";
                        $retval = mysql_query($sql,$this->conn);
                        if(!$retval)
                        {
                            die('Non posso inserire i dati. Descrizione Errore: ' . mysql_error());
                        }
                        mysql_close($this->conn);
                        echo '<script type="text/javascript">window.location = "installer.class.php";</script>';

                        break;
                    case "photoprofile":
                        echo '

                        <form class="form-wrapper-01" enctype="multipart/form-data" action="installer.class.php?step=0&procedure=photoprofile_process" method="POST">
                        <input class="inputbox" type="file" name="ppic" placeholder="Una tua foto" required><br />>
                        <button type="submit" class="cld">Prosegui</button>
                        ';
                    break;
                    case "photoprofile_process":
                        //RICHIEDO L'ID DELL'UTENTE
                        $id = $this->ShowId();

                        //MI CONNETTO AL DB E VERIFICO LA CONNESSIONE
                        $this->ConnectDb();

                        //CREO LE CARTELLE
                        $this->MkDir();
                        $userfolder = md5($id);


                        //LINK PHOTO PROFILE
                        $pict = $_FILES['ppic'];

                        // RECUPERO I PARAMETRI DA PASSARE ALLA FUNZIONE PREDEFINITA PER L'UPLOAD
                        $cartella = (__DIR__.'/../../storage/users/'.$userfolder.'/profile/');
                        $percorso = $pict['tmp_name'];
                        $nome = $pict['name'];
                        $cartellas = $nome;
                        // ESEGUO L'UPLOAD CONTROLLANDO L'ESITO
                        if (move_uploaded_file($percorso, $cartella . $nome))
                        {
                            print "Upload eseguito con successo";
                        }
                        else
                        {
                            print "Si sono verificati dei problemi durante l'Upload";

                            echo $percorso;

                        }

                        $sql="UPDATE settings SET sec_picture='$cartellas' WHERE id='$id'";
                        $retval1 = mysql_query($sql,$this->conn);
                        if(!$retval1)
                        {
                            die('Non posso inserire i dati. Descrizione Errore: ' . mysql_error());
                        }
                        mysql_close($this->conn);


                        //RICHIAMO FUNZIONI PRINCIPALI PER GESTIONE DB, ID E CREAZIONE CARTELLE PER UTENTE


                        //FINE CARICAMENTO IMMAGINE E LINK A SECONDA PROCEDURA DI CARICAMENTO DELLE PREFERENZE
                        echo '<script type="text/javascript">window.location = "installer.class.php";</script>';


                        break;
                }
            break;

            case 1:
                switch($_GET['number']) {
                    case 1:
                $id= $this->ShowId();
                $this->ConnectDb();
                ## VARIABILE PER GESTIONE OUTPUT
                $sql = "REPLACE INTO follow_sites SET id='$id'";
                        $retval2 = mysql_query($sql,$this->conn);
                        if(!$retval2)
                        {
                            die('Non posso inserire i dati. Descrizione Errore: ' . mysql_error());
                        }

                        #TODO: Inserire qui tutte le opzioni ai diversi Feed
                        echo '
                        <form action="installer.class.php?step=1&number=2" method="POST">
                        <table style="float:left;">
                            <p style="text-align:left;"><input value="1" type="checkbox" name="f_corriere" id="corriere" /><label for="corriere"><span class="ui"></span>&nbsp;&nbsp;<b> Segui Il Corriere della Sera</b></label></p>
                            <p style="text-align:left;"><input value="1" type="checkbox" name="f_lastampa" id="lastampa" /><label for="lastampa"><span class="ui"></span>&nbsp;&nbsp;<b> Segui La Stsmpa</b></label></p>
                            <p style="text-align:left;"><input value="1" type="checkbox" name="f_larepubblica" id="larepubblica" /><label for="larepubblica"><span class="ui"></span> &nbsp;&nbsp;  <b> Segui La Repubblica</b></label></p>
                            <p style="text-align:left;"><input value="1" type="checkbox" name="f_androidworld" id="androidworld" /><label for="androidworld"><span class="ui"></span> &nbsp;&nbsp;  <b> Segui Android World</b></label></p>
                            <p style="text-align:left;"><input value="1" type="checkbox" name="tuttosport" id="ts" /><label for="ts"><span class="ui"></span> &nbsp;&nbsp;  <b> Segui TuttoSport</b></label></p>
                            <p style="text-align:left;"><input value="1" type="checkbox" name="ansa" id="4r" /><label for="4r"><span class="ui"></span> &nbsp;&nbsp;  <b> Segui  Ansa.it</b></label></p>

                            <br />

                        <button type="submit" class="cld">Invia</button>
                        </table>
                        </form>
                        ';
                        mysql_close();
                        break;
                    case 2:
                        $this->ConnectDb();
                        $id = $this->ShowId();
                            //GESTIONE CHECKBOX
                        if (((isset($_POST['f_larepubblica'])==1) AND ($_POST['f_larepubblica'])==1)) {
                            $repubblica = 1;
                        } else { $repubblica = 0; }

                        if (((isset($_POST['f_corriere'])==1) AND ($_POST['f_corriere'])==1)){
                            $corriere = 1;
                        } else { $corriere = 0; }

                        if (((isset($_POST['f_lastampa'])==1) AND ($_POST['f_lastampa'])==1)){
                            $stampa = 1;
                        } else { $stampa = 0; }

                        if ((isset($_POST['f_androidworld'])==1) AND ($_POST['f_androidworld']==1)) {
                            $aw = 1;
                        } else { $aw = 0; }

                        if ((isset($_POST['ansa'])==1) AND ($_POST['ansa']==1)) {
                            $fur = 1;
                        } else { $fur = 0; }

                        if ((isset($_POST['tuttosport'])==1) AND ($_POST['tuttosport']==1)) {
                            $tts = 1;
                        } else { $tts = 0; }

                        ## VARIABILE PER GESTIONE OUTPUT
                        $sql = "UPDATE follow_sites SET ansa='$fur', tuttosport='$tts' repubblica='$repubblica', stampa='$stampa', androidworld='$aw', corriere='$corriere' WHERE id='$id'";
                        $retval2 = mysql_query($sql,$this->conn);
                        if(!$retval2)
                        {
                            die('Non posso inserire i dati. Descrizione Errore: ' . mysql_error());
                        }

                        echo '<script type="text/javascript">window.location = "installer.class.php";</script>';

                }
            break;

            # INIZIALLIZZAZIONE
                # CASE PARTONO DA QUI ORA (-1 IDENTAZIONE)
                case 2:
                    $id = $this->ShowId();
                    $this->ConnectDb();

                    # IMPOSTO IL PARAMETRO keyl
                    $keyl = 1;

                    # CONTROLLA CONNESSIONE E RESTUISCE, SE NECESSARIO, CODICE ERRORE
                    $sql = "UPDATE users SET keyl='$keyl' WHERE id='$id'";
                    $retval2 = mysql_query($sql,$this->conn);
                    if(!$retval2)
                    {
                        die('Non posso inserire i dati. Descrizione Errore: ' . mysql_error());
                    }
                    mysqli_close($this->conn); # ERRORE SULLA CHIUSURA DELLA SESSIONE SQL, DA RICONTROLLARE IL CODICE

                    echo '<script type="text/javascript">window.location = "../../index.php";</script>';
                    break;
        }
}
}



#AVVIO LE ISTANZE NECESSARIE PER FAR FUNZIONARE IL PROCESSO DI INSTALLAZIONE
$chkSign = new Installer();
$chkSign->CheckSign($zero=0);

$chkVer = new Installer();
$chkVer->CheckVer($zero=0);

?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Installazione Quickl - Quickl</title>
    <link rel="stylesheet" type="text/css" href="../../ui/assets/css/installer.css"/>

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600,700' rel='stylesheet'
          type='text/css'>
    <script type="text/javascript" src="../global/js/modernizr.js"></script>
</head>


<body style="background-image:url('../../storage/wallpapers/loginRegister/1.jpg');">
<div style="background:rgba(0,0,0, 0.65); width:100%; height:100%;">
<div class="section" id="section2" style="width:100%;">
    <div class="login-form" style="margin-left: 50%; left:-200px; position:absolute;">
        <h1>Installazione</h1>

        <p>Stai Installando il tuo Quickl! ;)</p>

        <div>
            <?php


            $ins = new Installer();
            $ins->Installing();

            #AVVIO CODICE HTML
            $htmlCode = new Installer();
            $htmlCode->HtmlStep1();


            ?>

        </div>
    </div>
</div>

</body>
</html>
