<?php

Class FeedWebsite {
    public $conn;

    # CREO LA FUNZIONE ConnectDb() PER LA GESTIONE DEL DB E LA CONNESSIONE A QUESTO

    final protected function ConnectDb() {
        require (__DIR__ ."/../../registration/login/standard/db_config.php");

        $this->conn = mysql_connect($host,$user,$password) OR die("Impossibile connettersi al database");
        mysql_select_db($db, $this->conn);
    }

    # GESTIONE DELL'ID PER LE ALTRE CLASSI
    protected function ShowId()
    {
        $this->ConnectDb();
        $sql = "SELECT id FROM users WHERE id=$_SESSION[user_id]";
        $res = mysql_query($sql, $this->conn);
        $row = mysql_fetch_array($res);
        mysql_close($this->conn);
        return $row['id'];
    }

    protected function PartX($rss, $generator) {

        $feed = array();
        foreach ($rss->getElementsByTagName('item') as $node) {
            $item = array (
                'title' => $node->getElementsByTagName('title')->item(0)->nodeValue,
                'desc' => $node->getElementsByTagName('description')->item(0)->nodeValue,
                'link' => $node->getElementsByTagName('link')->item(0)->nodeValue,
                'date' => $node->getElementsByTagName('pubDate')->item(0)->nodeValue,
            );
            array_push($feed, $item);
        }
        $limit = 5; #MODIFICARE QUESTO CAMPO PER IL LIMITE DI POST

        for($x=0;$x<$limit;$x++) {
            $title = htmlentities(str_replace(' & ', ' &amp; ', $feed[$x]['title'])); ENT_XML1;
            $link = (htmlentities($feed[$x]['link']));
            $description = ($feed[$x]['desc']);
            $date = date('d-m-Y', strtotime($feed[$x]['date']));

            # CODICE CHE GESTICHE L'OUTPUT DELL'APPLICAZIONE
            echo '<div class="feed_container" id="'.$date.'">';
            echo '<p class="feed_title"><a href="'.$link.'" title="'.$title.'">'.$title.'</a></p>';
            echo '<p class="feed_date">Postato il '.$date.' da '. $generator . '</p>';
            echo '<p class="feed_description">'.$description.'</p><br /><br />';
            echo '</div>';
        }
    }

    ## LEGGO TUTTI I DIVERSI FEED RSS

   public function ReadFeedCorriere() {
    $rss = new DOMDocument();
    //qui mettiamo la pagina RSS creata tramite MySql (vedi articolo precedente)
    //ovviamente si può usare anche un file con estensione .rss
    $rss->load('http://xml.corriereobjects.it/rss/homepage.xml');
       $generator = "Il Corriere"; # DICO DA QUALE QUOTIDIANO HO RILEVATO LA NOTIZIA
    $this->PartX($rss, $generator);
    }

    public function ReadFeedStampa() {
        $rss = new DOMDocument();
        $rss->load('http://www.lastampa.it/rss.xml');
        $generator = "La Stampa";
        $this->Partx($rss, $generator);
    }

    public function ReadFeedAndroidWorld() {
        $rss = new DOMDocument();
        $rss->load('http://www.androidworld.it/feed/');
        $generator = "AndroidWorld";
        $this->Partx($rss, $generator);
    }

    public function ReadFeedRepubblica() {
        $rss = new DOMDocument();
        $rss->load('http://www.repubblica.it/rss/homepage/rss2.0.xml');
        $generator = "La Repubblica";
        $this->Partx($rss, $generator);
    }

    public function ReadFeedTuttoSport() {
        $rss = new DOMDocument();
        $rss->load('http://tuttosport.feedsportal.com/c/34178/f/619238/index.rss');
        $generator ="Tutto Sport";
        $this->PartX($rss, $generator);
    }

    public function ReadFeedAnsa() {
        $rss = new DOMDocument();
        $rss->load('https://www.ansa.it/main/notizie/awnplus/topnews/synd/ansait_awnplus_topnews_medsynd_Today_Idx.xml');
        $generator ="Ansa";
        $this->PartX($rss, $generator);
    }

    public function SelectFeed() {
        # AVVIO LE ISTANZE FONDAMENTALI: CONNESSIONE DB E CONTROLLO CONNESSIONE DB E VISTA ID DELL'UTENTE;
        $this->ConnectDb();

        ## TODO: DA FINIRE LA SEZIONE DI CODICE CHE GESTIRA' I SITI WEB E I SUOI FEED
        # AVVIO L'UTILIZZO DEL CODICE SQL
        $sql = "SELECT repubblica, corriere, androidworld, stampa, tuttosport, ansa FROM follow_sites WHERE id='$_SESSION[user_id]'";
        $res = mysql_query($sql, $this->conn);
        $quanti = mysql_num_rows($res);
        for($x=0; $x<$quanti; $x++)
        {
            $rs = mysql_fetch_row($res);
            $stampa = $rs[3];
            $repubblica = $rs[0];
            $androidworld = $rs[2];
            $corriere = $rs[1];
            $tuttosport = $rs[4];
            $ansa = $rs[5];
        }

        ## IF PER GESTIONE VISIONE FEED O MENO
        if ($stampa==1) {$this->ReadFeedStampa();}
        if ($corriere==1) {$this->ReadFeedCorriere(); }
        if ($androidworld == 1) {$this->ReadFeedAndroidWorld();}
        IF ($repubblica == 1) {$this->ReadFeedRepubblica();}
        if ($tuttosport == 1) {$this->ReadFeedTuttoSport();}
        if ($ansa == 1) {$this->ReadFeedAnsa();}

        ## LETTORE DEI FEED RSS



    }



    # GESTIONE SECONDARIA

    protected function Chkdir() {

    }
}