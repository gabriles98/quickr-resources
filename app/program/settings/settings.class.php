<?php

class Settings {

    //FUNZIONI PUBBLICHE
    public $conn;
    public $zero = 0;

    # CREO LA FUNZIONE ConnectDb() PER LA GESTIONE DEL DB E LA CONNESSIONE A QUESTO
    final protected function ConnectDb() {
    require (__DIR__ ."/../../registration/login/standard/db_config.php");

    $this->conn = mysql_connect($host,$user,$password) OR die("Impossibile connettersi al database");
    mysql_select_db($db, $this->conn);
}

    # GESTIONE DELL'ID PER LE ALTRE CLASSI
    protected function ShowId()
    {
        $this->ConnectDb();
        $sql = "SELECT id FROM users WHERE id=$_SESSION[user_id]";
        $res = mysql_query($sql, $this->conn);
        $row = mysql_fetch_array($res);
        mysql_close($this->conn);
        return $row['id'];
    }


    # GESTIONE DEL NOME E COGNOME

        //MOSTRO IL NOME
    public function ShowName() {
        $this->ConnectDb();
        $sql = "SELECT nome FROM settings WHERE id=$_SESSION[user_id]";
        $res = mysql_query($sql, $this->conn);
        $row = mysql_fetch_array($res);
        mysql_close($this->conn);
        return $row['nome'];
    }
        //MOSTRO IL COGNOME
    public function ShowSurname() {
        $this->ConnectDb();
        $sql = "SELECT cognome FROM settings WHERE id=$_SESSION[user_id]";
        $res = mysql_query($sql, $this->conn);
        $row = mysql_fetch_array($res);
        mysql_close($this->conn);
        return $row['id'];
    }
        //MODIFICO IL CAMPO NOME
    public function ModifyName() {
        $this->ConnectDb();
        $sql = "UPDATE settings SET nome='$_POST[modify_name]' WHERE id=$_SESSION[user_id]";
        $res = mysql_query($sql, $this->conn);
        if(!$res)
        {
            die('Non posso inserire i dati. Descrizione Errore: ' . mysql_error());
        }
        mysql_close($this->conn);

    }
        //MODIFICO COGNOME
    public function ModifySurname() {
        $this->ConnectDb();
        $sql = "UPDATE settings SET cognome='$_POST[modify_surname]' WHERE id=$_SESSION[user_id]";
        $res = mysql_query($sql, $this->conn);
        if(!$res)
        {
            die('Non posso inserire i dati. Descrizione Errore: ' . mysql_error());
        }
        mysql_close($this->conn);


    }

    # GESTIONE DEL SESSO E DELL'ETA'
        //MOSTRA IL SESSO
    public function ShowSex() {
        $this->ConnectDb();
        $sql = "SELECT sesso FROM settings WHERE id=$_SESSION[user_id]";
        $res = mysql_query($sql, $this->conn);
        $row = mysql_fetch_array($res);
        if ($row == "m") {
            return "Maschio";
        }
        elseif ($row == "f") {
            return "Femmina";
        }
        else
        {
            return "Non so il tuo Sesso!";
        }
    }
        //NON E' POSSIBILE MODIFICARE IL SESSO DELLA PERSONA PER SCELTA DEL TEAM DI SVILUPPO

        //GESTIONE ETA' E DATA DI NASCITA
    public function ShowAge() {
        $this->ConnectDb();
        $sql = "SELECT age, eta FROM settings WHERE id=$_SESSION[user_id]";
        $res = mysql_query($sql, $this->conn);
        $row = mysql_fetch_array($res);
        $age = $row['eta'];
        $birth = $row['age'];
        $birthandage = $age . "( ". $birth . " ) ";
        # RICHIAMO LA FUNCIONE BIRTHANDAGE CHE CONTIENE LE DUE VARIABILI SULL'ETA
        return $birthandage;
    }
        //MODIFICA ETA'
    public function ModifyAge() {
        $id = $this->ShowId();
        $this->ConnectDb();
        $age = $_POST['modify_age'];

        function age($date){
            $year_diff = '';
            $time = strtotime($date);
            if(FALSE === $time){
                return '';

            }
            $date = date('Y-m-d', $time);
            list($year,$month,$day) = explode("-",$date);
            $year_diff = date("Y") - $year;
            $month_diff = date("m") - $month;
            $day_diff = date("d") - $day;
            if ($day_diff < 0 || $month_diff < 0)
            {
                $year_diff--;
            }
            elseif  ($day_diff > 0 || $month_diff > 0){

            }

            return $year_diff;
        }

        //SALVO NELLA VARIABILE $eta L'ETA' EFFETTIVA DELLA PERSONA
        $eta = age($_POST['modify_age']);

        //AVVIO CODICE SQL
        $sql="UPDATE settings SET age='$age', eta='$eta' WHERE id='$id'";
        $retval = mysql_query($sql,$this->conn);
        if(!$retval)
        {
            die('Non posso inserire i dati. Descrizione Errore: ' . mysql_error());
        }
    }


    # GESTIONE DELLE CARTELLE DELL'UTENTE PRESENTI NELLA FOLDER 'STORAGE/USERS'
    protected function FolderId() {
        $this->ConnectDb();
        $id = $this->ShowId();
        $sql = "SELECT folder FROM settings WHERE id=$_SESSION[user_id]";
        $res = mysql_query ($sql, $this->conn);
        $row = mysql_fetch_array($res);
        return $row['folder'];
    }

    # PREVISIONI METEO


    public function ShowProfilePic() {
        //Imposto la directory da leggere
        $directory = __DIR__."/../../../storage/users/".$this->FolderId()."/profile/";
        // Apriamo una directory e leggiamone il contenuto.
        if (is_dir($directory)) {
            //Apro l'oggetto directory
            if ($directory_handle = opendir($directory)) {
                //Scorro l'oggetto fino a quando non è termnato cioè false
                while (($file = readdir($directory_handle)) !== false) {
                    //Se l'elemento trovato è diverso da una directory
                    //o dagli elementi . e .. lo visualizzo a schermo
                    if((!is_dir($file))&($file!=".")&($file!=".."))
                        $dir=$file[0];
                }
                echo '<img src="'. $dir . "' />";
                //Chiudo la lettura della directory.
                closedir($directory_handle);
            }

        }
    }

}

class FunctionbySettings extends Settings {

}
?>
