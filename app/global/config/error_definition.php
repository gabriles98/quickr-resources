<?php
if(isset($_GET['alert']))
{
    echo '<script type="text/javascript">';
    switch($_GET['alert'])
    {
        case 1:
            echo 'alert("Tutti i campi devono essere compilati")';
            break;
        case 2:
            echo 'alert("Le due password inserite non corrispondono")';
            break;
        case 3:
            echo 'alert("L\'username inserito esiste già nel database")';
            break;
        case 4:
            echo 'alert("L\'email fornito esiste già nel database. Sei certo di non esserti già iscritto???")';
            break;
        case 5:
            echo 'alert("L\'indirizzo email fornito crea problemi")';
            break;
    }
    echo '</script>';
}
?>