<?php

class NewUser
{
    public $conn;

    public function AddUser()
    {
        $this->ErrorReport();
    }

    protected function DbConnect()
    {
        include(__DIR__ . "/../login/standard/db_config.php");

        $this->conn = mysql_connect($host,$user,$password) OR die("Impossibile connettersi al database");
        mysql_select_db($db, $this->conn);
    }

    protected function IsEmptyField()
    {
        if(empty($_POST['username']) OR empty($_POST['email']) OR empty($_POST['password']))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    protected function VerifyPassword()
    {
        if($_POST['password'] == $_POST['password2'])
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    protected function UsernameExists()
    {
        $this->DbConnect();
        $sql = "SELECT username FROM users WHERE username='$_POST[username]'";
        $res = mysql_query($sql, $this->conn);
        if($row = mysql_fetch_array($res))
        {
            mysql_close($this->conn);
            return TRUE;
        }
        else
        {
            mysql_close($this->conn);
            return FALSE;
        }
    }

    protected function EmailExists()
    {
        $this->DbConnect();
        $sql = "SELECT * FROM users WHERE email='$_POST[email]'";

        $res = mysql_query($sql, $this->conn);
        if($row = mysql_fetch_array($res))
        {
            mysql_close($this->conn);
            return TRUE;
        }
        else
        {
            mysql_close($this->conn);
            return FALSE;
        }
    }

    protected function VerifyEmail()
    {
        $pattern = "^([a-zA-Z0-9])+([a-zA-Z0-9]+[-_\.]?)*([a-zA-Z0-9])+(@)([a-zA-Z0-9])+([a-zA-Z0-9]+[-_\.]?)*([a-zA-Z0-9])+(\.[a-z]{2,4})$";

        if(ereg($pattern,$_POST['email']))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function ErrorResult($num)
    {
        header("Location: ../../../ui/login/loginRegister.php?alert=" . $num);
        die;
    }

    protected function ErrorReport()
    {
        if($this->IsEmptyField())
        {
            $this->ErrorResult(1);
        }

        if(!$this->VerifyPassword())
        {
            $this->ErrorResult(5);
        }

        if($this->UsernameExists())
        {
            $this->ErrorResult(3);
        }

        if($this->EmailExists())
        {
            $this->ErrorResult(4);
        }
        if(!$this->VerifyEmail())
        {
            $this->ErrorResult(6);
        }

        $this->InsertNewUser();

    }

    protected function GetKey()
    {
        $car = "aAbBcCdDeEfFgGhHiIlLjJkKmMnNoOpPqQrRsStTuUvVwWxXyYzZ0123456789";
        $dim = 40;
        srand((double)microtime()*1000000);
        $string = '' ;

        for($inc=0; $inc<$dim; $inc++)
        {
            $rand = rand(0, strlen($car)-1);
            $scar = substr($car, $rand, 1);
            $string = $string . $scar;
        }

        return $string;
    }

    protected function SendUserMail($key)
    {
        $email = $_POST['email'];
        $mittente = 'From: Quickl Team [noreply@quickl.com] \r\n';
        $destinatario = $email;
        $oggetto = "Conferma di Avvenuta Registrazione";
        $content = "Benvenuto $_POST[username],\r\n";
        $content .= "per confermare la tua iscrizione devi cliccare sul seguente link:\r\n\r\n";
        $content .= "http:/localhost/app/registration/signup/verify_user.php?key=" . $key;  //ATTENZIONE AL LINK QUI
        $messaggio = $content;
        mail($destinatario, $oggetto, $messaggio, $mittente);
        return;
    }

    protected function InsertNewUser()
    {
        $password = md5($_POST['password']);
        $key_control = $this->GetKey();

        $sql = "INSERT INTO users (username,email,password,key_control) VALUES ('$_POST[username]','$_POST[email]','$password','$key_control')";

        $this->DbConnect();

        mysql_query($sql,$this->conn);

        mysql_close($this->conn);

        $this->SendUserMail($key_control);
    }

    public function MkDir() {

    }

    public function VerifyUser()
    {
        $sql = "SELECT id FROM users WHERE key_control='$_GET[key]'";
        $this->DbConnect();
        $res = mysql_query($sql,$this->conn);

        if($row = mysql_fetch_array($res))
        {
            $query = "UPDATE users SET ver=1,key_control='0' WHERE id='$row[id]'";
            mysql_query($query,$this->conn);
            mysql_close($this->conn);
            echo "Il tuo account è ora attivato!";
        }
        else
        {
            echo "Impossibile verificare l'account!";
        }
    }
}


?>