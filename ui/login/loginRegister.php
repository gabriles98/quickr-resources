<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Area Accesso - Quickl</title>
        <link rel="stylesheet" type="text/css" href="../assets/css/login.css"/>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600,700' rel='stylesheet'
              type='text/css'>
    </head>

    <?php
        include (__DIR__ . "/../../app/registration/signup/error_newuser.php"); //ERRORI DERIVATI DA NEWUSER.CLASS.PHP E DA INSTALLER.CLASS.PHP E DA SOCIAL_INSTALL.CLASS.PHP
    ?>

    <body id="body">
        <div class="postbody">
        <!--Login Section-->

        <div class="section" id="section1">
            <div class="login-form">
                <h1>Login</h1>

                <p>Hai gi&agrave; un account? Perfetto! Accedi ;)</p>

                <div>
                    <form class="form-wrapper-01" id="formLogin" action="../../app/registration/login/standard/verify_login.php" method="POST">
                        <input name="email" id="" class="inputbox email" type="text" placeholder="La tua mail"/>
                        <input name="password" id="" class="inputbox password" type="password" placeholder="Password"/>
                        <p><a href="#" onclick="$('#formLogin').submit()" class="button">Login <i class="icon-paper-plane"></i></a></p>
                    </form>
                    <p>Dimenticato la password? Tutto ok!<br /> <a class="scroll" href="#section3">Richiedicela qui &raquo;</a></p>
                </div>
                <hr/>
                <p>O tu puoi accedere con uno dei seguenti servizi:</p>

                <div class="social"><a href="#" class="facebook"><?php require (__DIR__. "/../../app/install/facebook.class.php"); ?><i class="icon-facebook"></i></a>
                    <a href="#" class="twitter"><i class="icon-twitter"></i></a> <a href="#" class="google"><i class="icon-gplus"></i></a></div>
                <p>Non hai un account? <a class="scroll" href="#section2">Registrati ora! &raquo;</a></p>
            </div>
        </div>
        <!--END: Login Section-->
        <!--Signup Section-->
        <div class="section" id="section2">
            <div class="signup-form">
                <h1>Registrati facilmente!</h1>

                <p>Registrazione con Email</p>

                <div>
                    <?php

                    ?>.
                    <form class="form-wrapper-01" action="register.php" method="post">
                        <input id="username" name="username" class="inputbox name" type="text" placeholder="Il tuo nome"/>
                        <input id="email" name="email" class="inputbox email" type="text" placeholder="La tua Email"/>
                        <input id="password" name="password" class="inputbox password" type="password" placeholder="Password"/>
                        <input id="password2" name="password2" class="inputbox password" type="password" placeholder="Conferma Password"/>

                        <button type="submit" class="cld">Crea il mio Account!</button> <!-- NON TOCCARE -->
                        <!-- <input id="" type="button" class="button" value="Sign up" /> -->
                    </form>
                </div>
                <hr/>

            </div>
        </div>
        <!--END: Signup Section-->
        <!--Forget Password Section-->
        <div class="section" id="section3">
            <div class="login-form">
                <h1>Password Dimenticata?</h1>

                <p>Ohh, Non ti preoccupare! Ritrovala qui ;)</p>

                <div>
                    <form class="form-wrapper-01" action="../../app/registration/signup/send_pass.php" method="POST">
                        <input id="" name="email" class="inputbox email" type="text" placeholder="Email id"/>
                        <button type="submit" class="cld">Invia!</button> <!-- NON MODIFICARE IL CODICE -->
                    </form>
                </div>
                <hr/>
                <p>Ti sei ricordato d'improvviso la Password? Grande!!</p>

                <p><a class="scroll" href="#section1">&laquo; Loggati qui</a></p>
            </div>

        </div>
        <!--END: Forget Password Section-->
        <!--Script for Horizontal Scrolling-->
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
        <script type="text/javascript" src="../assets/js/jquery.easing.1.3.js"></script>
        <script type="text/javascript" src="../assets/js/function.js"></script>
        <script type="text/javascript">
            $(function () {
                $('a.scroll').bind('click', function (event) {
                    var $anchor = $(this);
                    $('html, body').stop().animate({
                        scrollLeft: $($anchor.attr('href')).offset().left
                    }, 500, 'easeInOutExpo');
                    /* Uncomment this for another scrolling effect */
                    /*
                     $('html, body').stop().animate({
                     scrollLeft: $($anchor.attr('href')).offset().left
                     }, 1000);*/
                    event.preventDefault();
                });
                centraSection();
            });
        </script>

    </div>
    </body>
</html>