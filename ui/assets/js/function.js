function centraSection() {
    var widthBody = $(window).width();
    widthBody = widthBody / 2;
    $('.section > div').each(function( index ) {
        var widthSection = $(this).width();
        widthSection = widthSection / 2;
        var marginSection = widthBody - widthSection;
        $(this).css({
           "margin-left" : marginSection
        });
    });
}
$( window ).resize(function() {
    centraSection();
});