<?php
header('Content-type: text/html;charset=utf-8');
?>
<div class="news_container">
    <?php
    require (__DIR__.'/../../../app/program/news/feed.class.php');
    ###### ISTRUZIONI ALL'UTILIZZO DELLA SEZIONE feed.class.php CORRELATA A news_container.php
    ## 1. FUNZIONI BASE PER L'UTILIZZO DEI FEED
      /*
       * Per visualizzare i Feed RSS bisogna importare la classe e la relativa libreria, con la seguente funzione:
       *
       *    $feedAccess = New FeedWebsite($limit);
       *
       * Questa è la funzione che richiama la classe FeedWebsite e la inizializza. Per accedere, poi, alla variabile
       * che gestirà la connessione ai Feed e la visualizzazione dei feed in formato HTML è:
       *
       *    $feedAccess->ReadFeed($limit=50);
       *
       * In questo modo richiamo la variabile $feedAccess() che ha inizializzato la classe relativa e le sue librerie
       * e gli dico di far avviare la funzione ReadFeed con limite massimo di lettura di 50 news per sito ($limit=50)
       * Per modificare il limite massimo basta modificare il valore della variabile $limit (es: ..ReadFeed($limit=5):
       * in questo caso leggerà solo le ultime 5 news disponibili per ogni sito web. Utilizzando questa impostazione
       * si potrà in AJAX modificare la variabile in base a quante news vuole leggere l'utente dal sito web per rendere
       * più dinamico lo scambio di informazioni UTENTE<->SERVER.
       *
       * ## ORA ANDIAMO A VEDERE l'OUTPUT DELLA CLASSE:
       *
       *    echo '<p class="feed_title"><a href="'.$link.'" title="'.$title.'">'.$title.'</a></p><br />';
       *    echo '<p class="feed_date">Postato il '.$date.'</p>';
       *    echo '<p class="feed_description">'.$description.'</p>';
       *
       * Abbiamo 3 echo che richiamano 3 diverse variabili. Nel primo £echo abbiamo un richiamo al link della news e al
       * suo titolo; nel secondo echo abbiamo la data di riferimento della news($date); e nel terzo £echo abbiamo il
       * sommario della news.
       *
       * Per modificare il CSS/SCSS dei valori dovremmo usare le classi:
       *    - "feed_title": per la visualizzazione del titolo
       *    - "feed_title > a ": per settare le impostazioni style del link
       *    - "feed_date": per settare la data e le sue impostazioni di stile
       *    - "feed_description": per modificare lo style della descrizione
       *    - "feed_container": è la classe CSS al <div> che contiene tutti gli altri parametri: Titolo, descriz., data
       *
       * Per modificare l'output accedere a feed.class.php nella sezione app>program>news e modificare i parametri dei
       * 3 echo disponibili. Non toccare le altre variabili e/o parametri senza previa richiesta.
       */


     ######## FUNZIONAMENTO SEZIONE SOCIAL
     #TODO Scrivere il funzionamento, verrà scritto appena pronte le prime funzioni per il social.

    ?>
    <?php
    # CLASSE PER FUNZIONAMENTO FEED RSS
    $feedAccess = New FeedWebsite();
    $feedAccess->SelectFeed();

    ?>
</div>