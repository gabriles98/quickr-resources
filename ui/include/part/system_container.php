<?php
    require_once (__DIR__. "/../../../app/program/settings/settings.class.php")
?>

<div class="system_container">
    <?php
    //FACCIO VEDERE LA FOTO DEL PROFILO
        $settings_start= new Settings();

    ## GUIDA PER L'UTILIZZO DELLE VARIABILI E DEI RICHIAMI
    #### Usare la funzione [echo $settings_start->ShowName();] per visualizzare il nome della persona
    #### Usare la funzione [echo $settings_start->ShowUsername();] per visualizzare il cognome della persona
    #### Usare la funzione [echo $settings_start->ShowAge();] per visualizzare età + data di nascita
    #### Usare la funzione [echo $settings_start->ShowSex();] per visualizzare il sesso della persona
    ########### FUNZIONI SPECIALI DI MODIFICA ############
    #### 1. MODIFICARE IL NOME
    /*
     * <form class="class" method="post" action="#">
     *      <input type="text" name="modify_name" placeholder="Modifica il Nome" />
     *      <input type="submit" onclick="<?php $settings_start->ModifyName(); ?>">
     * </form>
     *
     */
     #### 1.1 MODIFICARE IL COGNOME
    /*
     * <form class="class" method="post" action="#">
     *      <input type="text" name="modify_surname" placeholder="Modifica il Cognome" />
     *      <input type="submit" onclick="<?php $settings_start->ModifySurname(); ?>">
     * </form>
     *
     */
         ###### SI POSSONO USARE DEI VALORI COMBINATI PER ESEMPIO:
        /*
         * <form class="class" method="post" action="#">
         *      <input type="text" name="modify_surname" placeholder="<?php echo $settings_start->ShowSurname();" />
         *      <input type="submit" onclick="<?php $settings_start->ModifySurname(); ?>">
         * </form>
         *
         * In questo modo nel Placeholder l'utente vedrà il suo cognome e potrà modificarlo scrivendogli sopra e cliccando
         * su invio
         *
         */
     #### 1.2 MODIFICARE L'ETA'
        /*
         * <form class="class" method="post" action="#">
         *      <input type="date" name="modify_age" placeholder="EX" />  ## IMPORTANTE UTILIZZARE IL PARAMETRO date ##
         *      <input type="submit" onclick="<?php $settings_start->ModifyAge(); ?>">
         * </form>
         */
     #### 1.3 MODIFICARE L'IMMAGINE DEL PROFILO
        /*
         * <form class="class" method="post" action="#">
         *      <input type="file" name="modify_picture" placeholder="EX" />  ## IMPORTANTE UTILIZZARE IL PARAMETRO file ##
         *      <input type="submit" onclick="<?php $settings_start->ModifyProfilePicture(); ?>">
         * </form>
         */
         #### 1.3.1 VISUALIZZARE L'IMMAGINE DEL PROFILO
            /*
             * <img src="<?php echo $settings_start->ShowProfilePicture(); ?>" alt="TOWRITE" class="TOWRITE" />
             */
         #### 1.3.2 SI POSSONO COMBINARE L'IMMAGINE DEL PROFILO E LA MODIFICA
         ############ -----------------------------------------------------------------------------------------------
         ###### utilizzando le classi <?php echo $settings_start->ShowProfilePicture(); ?/> per visualizzare l'immagine
         ###### e la classe $settings_start->ModifyProfilePicture con il param 'name' del file da caricare uguale a
         ###### modify_picture e con il form su method="post".
         ########## PER MAGGIORI INFORMAZIONI SU QUESTA FUNZIONE GUARDARE settings.class.php NELLA SEZ. APP

    // NUOVE FUNZIONI A BREVE


    $MeteoShow = New Settings();
    $MeteoShow->ShowProfilePic();
    ?>


</div>